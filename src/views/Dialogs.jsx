import React from 'react'

import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';

export const ACCEPT = 'ACCEPT'
export const REFUSE = 'REFUSE'

export function LoginDialog({show, handleLogin, handleRegister, handleLostPassword}) {

    const renderLoginFooter = () => {
        return (
          <div>
            <Button label="Login" icon="pi pi-check" onClick={() => handleLogin()} autoFocus />
            <Button label="Password dimenticata" onClick={() => handleLostPassword()} style={{ marginRight: 0 }} className="p-button-text" />
            <Button label="Registrati" onClick={() => handleRegister(true)} style={{ marginRight: 0 }} className="p-button-text" />
          </div>
        );
    }

    return (
        <Dialog id="login-dialog"  header={() => null} visible={!show} style={{ width: '80%' }} footer={renderLoginFooter()} closable={false} onHide={() => {}}>
          <div className="p-grid">
            <div className="p-col-12 p-md-6">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">
                  <i className="pi pi-user"></i>
                </span>
                <InputText placeholder="Email" />
              </div>
            </div>
            <div className="p-col-12 p-md-6">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">
                  <i className="pi pi-key"></i>
                </span>
                <InputText placeholder="Password" type='password' />
              </div>
            </div>
          </div>
        </Dialog>
    )
}


export function RegisterDialog({show, handleRegister}){

    const renderRegisterFooter = () => {
        return (
          <div>
            <Button label="Accetta e continua" icon="pi pi-check" onClick={() => handleRegister(ACCEPT)} autoFocus />
            <Button label="Rifiuta ed esci" icon="pi pi-times" onClick={() => handleRegister(REFUSE)} style={{ marginRight: 0 }} className="p-button-text" />
          </div>
        );
      }

    return (
        <Dialog header="Registrazione" visible={show} style={{ width: '80%' }} footer={renderRegisterFooter()} closable={false} onHide={() => null}>
          <div className="p-grid">
            <div className="p-col-12 p-md-6">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">
                  <i className="pi pi-user"></i>
                </span>
                <InputText placeholder="Email" />
              </div>
            </div>
            <div className="p-col-12 p-md-6">
              <div className="p-inputgroup">
                <span className="p-inputgroup-addon">
                  <i className="pi pi-key"></i>
                </span>
                <InputText placeholder="Nuova password" type='password' />
              </div>
            </div>
            <div className="p-col-12 m-hvp-50">
              <h1>Terms and conditions</h1>
              <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda numquam facilis sequi, unde magni, expedita reprehenderit tempore laborum et architecto nihil voluptatibus voluptates quam sed, minus earum. Earum, id corrupti? Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt sint ab optio nisi mollitia doloremque possimus, tempora eveniet minus iusto eius officiis eum ea excepturi impedit quis nihil dolor est!
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda numquam facilis sequi, unde magni, expedita reprehenderit tempore laborum et architecto nihil voluptatibus voluptates quam sed, minus earum. Earum, id corrupti? Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt sint ab optio nisi mollitia doloremque possimus, tempora eveniet minus iusto eius officiis eum ea excepturi impedit quis nihil dolor est!
              </p>
            </div>
          </div>
        </Dialog>
    )
}

